// console.log("Good Morning!");

// [SECTION] JSON Objects
	// JSON stands for JavaScript Object Notation.
	// JSON is also used in other programming languages.
	// core JavaScript has built-in JSON Object that contains methods for parsing objects and converting into JavaScript Objects.
		// JSON Object - it means parsed
		// Stringified JSON Object - JSON objects na nakastringify
	// JSON is used for serializing/converting different data types.
/*
	SYNTAX:
	{
		"propertyA" : "valueA",
		"propertyB" : "valueB",
	}
*/

	// JSON Objects
	/*{
		"city" : "Quezon City",
		"province" : "Manila",
		"country" : "Philippines",
	}*/

	// JSON Arrays
		// arrays of JSON objects
	/*[
		{
			"city" : "Quezon City",
			"province" : "Manila",
			"country" : "Philippines",
		},
		{
			"city" : "Manila City",
			"province" : "Manila",
			"country" : "Philippines",
		}

	]*/


// [SECTION] JSON Methods
	// contains methods for parsing and converting data into stringified JSON

	let batchesArr = [
			{
				batchName : "Batch X"
			},
			{
				batchName : "Batch Y"
			}
		]
	console.log("This is the original array:");
	console.log(batchesArr);

		// The "STRINGIFY METHOD" is used to convert JavaScript Objects into a string.
	let stringsBatchesArr = JSON.stringify(batchesArr);

	console.log("This is the result of stringify method:");
	console.log(stringsBatchesArr);
	console.log("Data Type:");
	// Only converts data to string and doesnt mutate the original array
	console.log(typeof stringsBatchesArr); // To check the data type of the array after the stringify method

	console.log("This is the original array of objects:");
	console.log(batchesArr);

	let data = JSON.stringify({
		name: 'John',
		address: {
			city: 'Manila',
			country: 'Philippines'
		}
	})
	console.log(data); //stringified data


// [SECTION] Use stringify method with variables and is not hard-coded into an object that is being stringified, we can supply the value with a variable;

	// let firstName = prompt("What is your First Name?");
	// let lastName = prompt("What is your Last Name?");
	// let age = prompt("How young are you?");
	// let address = {
	// 	city: prompt("What city do you live?"),
	// 	country: prompt("Which country does your city belong?")
	// }

	// let otherData = JSON.stringify({
	// 	firstName,
	// 	lastName,
	// 	age,
	// 	address
	// })
	// console.log(otherData);


// [SECTION] Converting stringified JSON into JavaScript Objects
/*
	- objects are common data types used in application because of the complex data structures that can be created out of them.

	- information is commonly sent to application in stringified JSON and then converted back into objects.

	- this happens both for sending information to a backend application and sending information bac to frontend application. 
*/

	// the "PARSE METHOD" converts the stringified JSON into JSON objects.
	let objectBatchesArr = JSON.parse(stringsBatchesArr);
	console.log("This is the stringify version:")
	console.log(stringsBatchesArr);

	console.log("This is the result after the pasre method:")
	console.log(objectBatchesArr);
	console.log(typeof objectBatchesArr);

	let stringifiedObject = `{
		"name": "John",
		"age": 31,
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}`;

	console.log(JSON.parse(stringifiedObject));
